# Memory Manager

Memory engine to show proof of concept for different types of abstract
data types. A test file has been written to automatically test each data type.

## Installation

In the same directory as the files run

```bash
make
```

To run the program run

```bash
./a.out
```
