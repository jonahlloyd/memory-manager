a.out: main.o Stack.o Pool.o DEStack.o Test.o MemManager.o
	g++ main.o Stack.o Pool.o DEStack.o Test.o MemManager.o

main.o: main.cpp
	g++ -c main.cpp

Stack.o: Stack.cpp Stack.h
	g++ -c Stack.cpp

Pool.o: Pool.cpp Pool.h
	g++ -c Pool.cpp

DEStack.o: DEStack.cpp DEStack.h
	g++ -c DEStack.cpp

Test.o: Test.cpp Test.h
	g++ -c Test.cpp

MemManager.o: MemManager.cpp MemManager.h
	g++ -c MemManager.cpp
