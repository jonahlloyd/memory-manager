#include <iostream>

#include "MemManager.h"

using namespace std;

MemManager::MemManager() = default;

// Stack Memory Management

Stack MemManager::CreateStack(size_t size){
    Stack stack(size);
    return stack;
}

size_t MemManager::ReturnStackMarker(Stack *stack) {
    size_t marker = stack->GetMarker();
    return marker;
}

void MemManager::PopStackToMarker(size_t marker, Stack *stack) {
    stack->FreeToMarker(marker);
}

void MemManager::ClearStack(Stack *stack) {
    stack->Clear();
}

void MemManager::FreeStack(Stack *stack) {
    stack->Free();
}

// Double Ended Stack Memory Management

DEStack MemManager::CreateDE(size_t stack_size) {
    DEStack stack(stack_size);
    return stack;
}

size_t MemManager::ReturnDEMarkerUpper(DEStack *stack) {
    size_t marker = stack->GetMarkerUpper();
    return marker;
}

size_t MemManager::ReturnDEMarkerLower(DEStack *stack) {
    size_t marker = stack->GetMarkerLower();
    return marker;
}

void MemManager::PopToDEMarkerUpper(size_t marker, DEStack *stack) {
    stack->FreeToMarkerUpper(marker);
}

void MemManager::PopToDEMarkerLower(size_t marker, DEStack *stack) {
    stack->FreeToMarkerLower(marker);
}

void MemManager::ClearDE(DEStack *stack) {
    stack->Clear();
}

void MemManager::FreeDE(DEStack *stack) {
    stack->Free();
}

// Pool Memory Manager

Pool MemManager::CreatePool(size_t n_chunks, size_t chunk_size) {
    Pool pool(n_chunks, chunk_size);
    return pool;
}

void MemManager::RemoveBlock(void *p_block, Pool pool) {
    pool.Deallocate(p_block);
}

void MemManager::FreePool(Pool pool) {
    pool.Free();
}
