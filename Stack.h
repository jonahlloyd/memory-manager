//
// Created by jonah on 04/11/2019.
//

#ifndef STACK_H
#define STACK_H

using namespace std;

class Stack {
public:
    size_t stack_s;

    Stack(size_t nBytes);
    void *Allocate(size_t size_bytes);
    size_t GetMarker();
    void FreeToMarker(size_t marker);
    void Clear();
    void Free();

private:

    void* stack;
    size_t p_top;
    size_t stack_size;
};


#endif
