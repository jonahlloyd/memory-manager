//
// Created by jonah on 04/11/2019.
//

#ifndef UNTITLED_TEST_H
#define UNTITLED_TEST_H

#include "MemManager.h"

class Test {
    public:

    MemManager memory_test;

    Test();
    void TestStack();
    void TestDEStack();
    void TestPool();
};


#endif
