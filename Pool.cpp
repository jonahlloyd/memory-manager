#include <iostream>

#include "Pool.h"

Pool::Pool(size_t nChunks, size_t chunkSize) {
    pool_size = nChunks * chunkSize;
    pool = malloc(pool_size);
    p_tail = &pool;
    // Pass value from private variable to public variable
    tail_pointer = p_tail;
    chunk_size = chunkSize;
    // Pass private variable value to pubic variable
    chunk_s = chunk_size;
    n_chunks = nChunks;

    // Cast void * to void **
    p_tail = static_cast<void **>(pool);
    // Get end of memory address
    void *end_address = p_tail + (chunk_size * n_chunks);

    // Place pointers to the next block throughout the pool
    for (int i = 0; i < n_chunks; ++i) {
        void *current_address = p_tail + (chunk_size * n_chunks);
        void *next_address = (void *) ((size_t) current_address + chunk_size);

        void **current_memory = (void **) current_address;
        if (next_address >= end_address) {
            *current_memory = nullptr;
        } else {
            *current_memory = (void *) next_address;
        }
    }
}


void *Pool::Allocate() {
if (p_tail != nullptr) {
    void *p_block = p_tail;
    p_tail = (void **) *p_tail;
    //For public variable
    tail_pointer = p_tail;
    
    return p_block;
}
else{
    cerr<<"Out of memory!"<<'\n';
    return nullptr;
}
}

void Pool::Deallocate(void *p_block) {
    if (p_block == nullptr) {
        // No data so return
        return;
    }
    // Check if list is empty
    if (p_tail == nullptr){
        p_tail = (void **)p_tail;
        *p_tail = nullptr;
    }
    else {
        void **freed_block = p_tail;
        p_tail = (void **)p_block;
        // For public variable
        tail_pointer = p_tail;
        *p_tail = (void *)freed_block;
    }
}

void Pool::Free(){
    free(pool);
}
