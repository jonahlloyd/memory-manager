//
// Created by jonah on 04/11/2019.
//

#include <iostream>
#include <assert.h>

#include "Test.h"

Test::Test(){}

void Test::TestStack() {
    std::cout<<"Testing Stack"<<endl;
    std::cout<<"\nCreating Stack..."<<endl;
    Stack test_stack = memory_test.CreateStack(20);
    Stack *p_stack = &test_stack;
    assert(p_stack->stack_s == 20);
    std::cout<<"Creation Successful."<<endl;

    std::cout<<"Adding Bytes to Stack..."<<endl;
    void *push_1 = memory_test.PushToStack(5, p_stack);
    size_t marker_1 = memory_test.ReturnStackMarker(p_stack);
    void *push_2 = memory_test.PushToStack("test", p_stack);
    void *push_3 = memory_test.PushToStack(1.5, p_stack);
    std::cout<<"Success."<<endl;

    // Attempt to add more than capacity of stack
    std::cout<<"Attmpting to Overload Stack..."<<endl;
    assert(!memory_test.PushToStack(5.0, p_stack));
    std::cout<<"Successfully Prevented Overload."<<endl;

    std::cout<<"Rolling Back Stack to Marker..."<<endl;
    size_t marker_2 = memory_test.ReturnStackMarker(p_stack);
    memory_test.PopStackToMarker(marker_1, p_stack);
    size_t marker_3 = memory_test.ReturnStackMarker(p_stack);

    // Assert stack has rolled back to correct marker
    assert(marker_1 == marker_3);
    std::cout<<"Success."<<endl;

    std::cout<<"Clearing Stack..."<<endl;
    memory_test.ClearStack(p_stack);
    size_t marker_4 = memory_test.ReturnStackMarker(p_stack);

    assert(marker_4 == 0);
    std::cout<<"Success."<<endl;

    memory_test.FreeStack(p_stack);
}

void Test::TestDEStack() {
    std::cout<<"\nTesting Double Ended Stack."<<endl;
    std::cout<<"\nCreating Double Ended Stack..."<<endl;
    DEStack test_stack = memory_test.CreateDE(25);
    DEStack *p_stack = &test_stack;

    assert(p_stack->stack_s == 25);
    std::cout<<"Creation Successful."<<endl;

    // Testing markers for upper end of DE stack
    std::cout<<"Adding Bytes to Stack..."<<endl;
    void *push_upper_1 = memory_test.PushToDEUpper(8.5, p_stack);
    size_t marker_upper_1 = memory_test.ReturnDEMarkerUpper(p_stack);
    void *push_upper_2 = memory_test.PushToDEUpper("hello", p_stack);
    size_t marker_upper_2 = memory_test.ReturnDEMarkerUpper(p_stack);
    memory_test.PopToDEMarkerUpper(marker_upper_1, p_stack);
    size_t marker_upper_3 = memory_test.ReturnDEMarkerUpper(p_stack);

    assert(marker_upper_1 == marker_upper_3);
    std::cout<<"Success."<<endl;

    // Testing markers for lower end of DE stack
    void *push_lower_1 = memory_test.PushToDELower(10.4, p_stack);
    size_t marker_lower_1 = memory_test.ReturnDEMarkerLower(p_stack);
    void *push_lower_2 = memory_test.PushToDELower(3.1, p_stack);

    // Attempt to add more than capacity of stack
    std::cout<<"Attempting to Overload Stack..."<<endl;
    assert(!memory_test.PushToDELower(2.5, p_stack));
    std::cout<<"Succesfully Prevented Overload."<<endl;

    // Rolling back markers in stack
    std::cout<<"Rolling Back Stack..."<<endl;
    size_t marker_lower_2 = memory_test.ReturnDEMarkerLower(p_stack);
    memory_test.PopToDEMarkerLower(marker_lower_1, p_stack);
    size_t marker_lower_3 = memory_test.ReturnDEMarkerLower(p_stack);

    assert(marker_lower_1 == marker_lower_3);
    std::cout<<"Success."<<endl;

    // Test that the stack clears correctly
    std::cout<<"Clearing Stack..."<<endl;
    memory_test.ClearDE(p_stack);
    size_t marker_upper_4 = memory_test.ReturnDEMarkerUpper(p_stack);
    size_t marker_lower_4 = memory_test.ReturnDEMarkerLower(p_stack);

    assert(marker_lower_4 == 0 && marker_upper_4 == p_stack->stack_s);
    std::cout<<"Success."<<endl;

    memory_test.FreeDE(p_stack);
}

void Test::TestPool() {
    std::cout<<"\nTesting Pool"<<endl;
    std::cout<<"\nCreating Pool Memory..."<<endl;
    Pool test_pool = memory_test.CreatePool(10, 4);

    assert(test_pool.pool_size == (10 * 4));
    std::cout<<"Creation Successful."<<endl;

    // Test that data type too large for created pool (float = 8 bytes)
    std::cout<<"Attempting to Add Data Too Large For Pool Chunks..."<<endl;
    assert(!memory_test.AddBlock(5.0, test_pool));
    std::cout<<"Successful Prevention."<<endl;

    // Test that blocks are added and removed correctly
    std::cout<<"Adding and Removing Blocks of Data..."<<endl;
    void *block_1 = memory_test.AddBlock(3, test_pool);
    void *block_2 = memory_test.AddBlock(10, test_pool);
    memory_test.RemoveBlock(block_1, test_pool);
    void *block_3 = memory_test.AddBlock('ok', test_pool);

    assert(block_2 == block_3);

    std::cout<<"Success."<<endl;

    memory_test.FreePool(test_pool);
}
