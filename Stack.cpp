//
// Created by jonah on 04/11/2019.
//

#include <iostream>

#include "Stack.h"

using namespace std;

Stack::Stack(size_t nBytes){
    stack_size = nBytes;
    stack = malloc(stack_size);
    //Private value of stack_size given to public value stack_s
    stack_s = stack_size;
    p_top = 0;
}

//Allocate space for data, increase pointer position
void *Stack::Allocate(size_t size_bytes){
    if(size_bytes + p_top > stack_size){
        throw "Stack: Not enough space on stack!";
    }
    //Critical section
//#pragma omp critical
    size_t address = ((size_t) stack) + p_top;
    //Increase top pointer
    p_top += size_bytes;

    return (void *) address;
}

//Place a marker in the stack
size_t Stack::GetMarker(){
    return p_top;
}

//Revert to a previous marker
void Stack::FreeToMarker(size_t marker){
    p_top = marker;
}

//Clear the the stack
void Stack::Clear(){
    p_top = 0;
}

//Free the stack from memory
void Stack::Free(){
    free(stack);
}
