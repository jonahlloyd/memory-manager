#include <iostream>

#include "DEStack.h"

using namespace std;

DEStack::DEStack(size_t nBytes){
    stack_size = nBytes;
    stack = malloc(stack_size);
    p_top_upper = stack_size;
    p_top_lower = 0;
    // Private variable stack_size value given to public variable stack_s
    stack_s = stack_size;
}

// Allocate space for data for upper end of stack, decrease pointer position
void* DEStack::AllocateUpper(size_t size_bytes){
    if((p_top_upper - size_bytes) < p_top_lower){
        throw "DEStack: Not enough space on stack!";
    }
    size_t address = ((size_t) stack) + stack_size - p_top_upper;
    // Decrease top pointer for upper
    p_top_upper -= size_bytes;

    return (void*) address;
}

// Allocate space for data for lower end of stack, increase pointer position
void* DEStack::AllocateLower(size_t size_bytes){
    if((p_top_lower + size_bytes) > p_top_upper){
        throw "DEStack: Not enough space on stack!";
    }
    size_t address = ((size_t) stack) + p_top_lower;
    // Increase top pointer for lower
    p_top_lower += size_bytes;

    return (void*) address;
}

// Place a marker in the stack for upper
size_t DEStack::GetMarkerUpper(){
    return p_top_upper;
}

// Place a marker in the stack for lower
size_t DEStack::GetMarkerLower(){
    return p_top_lower;
}

// Revert to a previous marker
void DEStack::FreeToMarkerUpper(size_t marker){
    p_top_upper = marker;
}

void DEStack::FreeToMarkerLower(size_t marker){
    p_top_lower = marker;
}

// Clear the the stack
void DEStack::Clear(){
    p_top_upper = stack_size;
    p_top_lower = 0;
}

// Free the stack from memory
void DEStack::Free(){
    free(stack);
}
