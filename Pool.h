//
// Created by jonah on 07/11/2019.
//

#ifndef POOL_H
#define POOL_H

using namespace std;

class Pool {
public:
    size_t pool_size;
    size_t chunk_s;
    void **tail_pointer;

    Pool(size_t nChunks, size_t chunkSize);
    void *Allocate();
    void Deallocate(void *p_block);
    void Free();

private:
    size_t n_chunks;
    size_t chunk_size;
    void *pool;
    void **p_tail;
};

#endif
