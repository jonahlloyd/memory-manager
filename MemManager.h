//
// Created by jonah on 07/11/2019.
//

#include "Stack.h"
#include "DEStack.h"
#include "Pool.h"

#ifndef MEMMANAGER_H
#define MEMMANAGER_H

class Stack;
class DEStack;
class Pool;

class MemManager{
public:
    MemManager();
    // Stack
    Stack CreateStack(size_t size);
    template<class T>
    void *PushToStack(T element, Stack *stack);
    size_t ReturnStackMarker(Stack *stack);
    void PopStackToMarker(size_t marker, Stack *stack);
    void ClearStack(Stack *stack);
    void FreeStack(Stack *stack);

    // Double Ended Stack
    DEStack CreateDE(size_t stack_size);
    template<class T>
    void *PushToDEUpper(T element, DEStack *stack);
    template<class T>
    void *PushToDELower(T element, DEStack *stack);
    size_t ReturnDEMarkerUpper(DEStack *stack);
    size_t ReturnDEMarkerLower(DEStack *stack);
    void PopToDEMarkerUpper(size_t marker, DEStack *stack);
    void PopToDEMarkerLower(size_t marker, DEStack *stack);
    void ClearDE(DEStack *stack);
    void FreeDE(DEStack *stack);

    // Pool
    Pool CreatePool(size_t n_chunks, size_t chunk_size);
    template<class T>
    void *AddBlock(T element, Pool pool);
    void RemoveBlock(void *p_block, Pool pool);
    void FreePool(Pool pool);

};

// Methods that use templates
template <class T>
void *MemManager::PushToStack(T element, Stack *stack) {
    try {
        stack->Allocate(sizeof(element));
    } catch (const char* msg){
        cerr<<msg<<endl;
    }
}

template<class T>
void *MemManager::PushToDEUpper(T element, DEStack *stack) {
    stack->AllocateUpper(sizeof(element));
}

template<class T>
void *MemManager::PushToDELower(T element, DEStack *stack) {
    try {
        stack->AllocateLower(sizeof(element));
    } catch (const char* msg){
        cerr<<msg<<endl;
    }
}

template<class T>
void *MemManager::AddBlock(T element, Pool pool) {
    // Attempt to add element as a block, throw exception if not
    try {
        if (sizeof(element) > pool.chunk_s) {
            throw "Pool: No space available for data!";
        }
        pool.Allocate();
    } catch (const char* msg){
        cerr<<msg<<endl;
    }
}


#endif
