#include <iostream>

#include "Test.h"
#include "MemManager.h"


int main() {
    Test test;
    test.TestStack();
    test.TestDEStack();
    test.TestPool();

    return 0;
}