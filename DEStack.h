//
// Created by jonah on 04/11/2019.
//

#ifndef DESTACK_H
#define DESTACK_H


class DEStack {
public:

    size_t stack_s;

    DEStack(size_t nBytes);
    void* AllocateUpper(size_t size_bytes);
    void* AllocateLower(size_t size_bytes);
    size_t GetMarkerUpper();
    size_t GetMarkerLower();
    void FreeToMarkerUpper(size_t marker);
    void FreeToMarkerLower(size_t marker);
    void Clear();
    void Free();

private:

    void* stack;
    size_t p_top_upper;
    size_t p_top_lower;
    size_t stack_size;
};

#endif